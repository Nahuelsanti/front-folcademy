import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './components/routes/contact/contact.component';
import { HomeComponent } from './components/routes/home/home.component';
import { ServicesComponent } from './components/routes/services/services.component';
import { EquiposComponent } from './components/routes/equipos/equipos.component';
import { GaleriaComponent } from './components/routes/galeria/galeria.component';
import { TalleresComponent } from './components/routes/talleres/talleres.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'services',
        component: ServicesComponent
      },
      {
        path: 'equipos',
        component: EquiposComponent
      },
      {
        path: 'galeria',
        component: GaleriaComponent
      },
      {
        path: 'talleres',
        component: TalleresComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
