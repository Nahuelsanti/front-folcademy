import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: any;
  error: boolean = false;

  registerForm = new FormGroup(
    {
      name: new FormControl ('',Validators.required),
      email: new FormControl ('',[Validators.required,Validators.email]),
      password: new FormControl ('',[Validators.required,Validators.minLength(8)])
    }
  )

  constructor(
    private _authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  register(){
    this._authService.register(this.registerForm.value).subscribe(
      response =>{
        this.user = response;
      }, error=>{
        console.log(error);
        this.error = true;
      }
    )
  }

}
