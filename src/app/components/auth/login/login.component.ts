import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: boolean = false;
  user: any;

  loginForm = new FormGroup(
    {
      email: new FormControl ('',[Validators.required,Validators.email]),
      password: new FormControl ('',[Validators.required,Validators.minLength(8)])
    }
  )
//  email: string= '';
//  password: string= '';

  constructor(
    private _authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  login(){
    this._authService.login(this.loginForm.value).subscribe(
      response =>{
        this.user = response;
        localStorage.setItem('user',JSON.stringify(this.user));
        alert(localStorage.getItem('user')+('USTED HA INICIADO SESION'));
      }, error=>{
        console.log(error);
        this.error = true;
      }
    )
  }

}
