import { Component, OnInit } from '@angular/core';

export interface cardTalleres{
  cardStyle? : string,
  imgTaller? : string,
  titleTaller? : string,
  contentTaller? : string,
  buttonTaller? : string | number,
  bandera? : boolean
}

@Component({
  selector: 'app-talleres',
  templateUrl: './talleres.component.html',
  styleUrls: ['./talleres.component.scss']
})
export class TalleresComponent implements OnInit {
  
  jumNewTaller ="Nuevo Taller de Pastoral Scout";
  jumDesTaller = "Te brindamos una experiencia nueva en donde podras aprender sobre las herramientas que tiene la Pastoral Scout";
  jumSubTaller = "No pierdas el tiempo, hay cupos limitados y una nueva aventura esperandote";
  urlInscripcion = "http://eut.edu.ar/archivo/curso-virtual-que-patrulla-la-de-los-apostoles-2/";
  jumMen = "Usted será redirigido a la página de inscripción. Haga click en 'Aceptar'. Gracias por visitarnos.";


  cardTalleres : Array <cardTalleres> = [
    {
      cardStyle : "Tallereslid",
      imgTaller : "../../../../assets/people-carry-solid.svg",
      titleTaller : "Taller de Liderazgo",
      contentTaller : "Aprende a como liderar un equipo con las '50 virtudes de un líder con San Pablo'.",
      buttonTaller : "Próximamente",
      bandera : true
    },
    {
      cardStyle : "Tallerespas",
      imgTaller : "../../../../assets/hiking-solid.svg",
      titleTaller : "Taller de Pastora de Juventud",
      contentTaller : "Construye una pastoral planificada desde la mirada de los jóvenes, y que sean parte.",
      buttonTaller : "Próximamente",
      bandera : true
    },
    {
      cardStyle : "Talleresnat",
      imgTaller : "../../../../assets/campground-solid.svg",
      titleTaller : "Taller de Naturaleza",
      contentTaller : "Reflexiones y actividades para la vida al aire libre. Contempla la belleza que te rodea.",
      buttonTaller : "Próximamente",
      bandera : true
    },
    {
      cardStyle : "Talleres",
      imgTaller : "../../../../assets/campground-solid.svg",
      titleTaller : "Taller de ¿¿¿???",
      contentTaller : "Reflexiones y actividades para la vida al aire libre. Contempla la belleza que te rodea.",
      buttonTaller : "Próximamente",
      bandera : false
    },
    {
      cardStyle : "Talleres",
      imgTaller : "../../../../assets/campground-solid.svg",
      titleTaller : "Taller de ¿¿¿???",
      contentTaller : "Reflexiones y actividades para la vida al aire libre. Contempla la belleza que te rodea.",
      buttonTaller : "Próximamente",
      bandera : false
    },
    {
      cardStyle : "Talleres",
      imgTaller : "../../../../assets/campground-solid.svg",
      titleTaller : "Taller de ¿¿¿???",
      contentTaller : "Reflexiones y actividades para la vida al aire libre. Contempla la belleza que te rodea.",
      buttonTaller : "Próximamente",
      bandera : false
    }
  ]

  alertJum (jumMen:string) {
    alert(jumMen);
  }

  newMensaje(mensajeNewTaller: string){
    alert(mensajeNewTaller)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
