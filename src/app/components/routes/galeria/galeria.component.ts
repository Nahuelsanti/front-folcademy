import { Component, OnInit } from '@angular/core';

export interface contentGal {
  urlImgGal? : string,
  altGal? : number | string,
}

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  titleGal= "Galería";
  subGal = "Aquí vas a ver nuestros proyectos pastorales";

  contentGal : Array <contentGal> = [
    {
      urlImgGal : "../../../../assets/galeria 1.jpg",
      altGal : 1,
    },
    {
      urlImgGal : "../../../../assets/galeria 2.jpg",
      altGal : 2,
    },
    {
      urlImgGal : "../../../../assets/galeria 3.jpg",
      altGal : 3,
    },
    {
      urlImgGal : "../../../../assets/galeria 5.jpg",
      altGal : 5,
    },
    {
      urlImgGal : "../../../../assets/galeria 4.jpg",
      altGal : 4,
    },
    {
      urlImgGal : "../../../../assets/galeria 6.jpg",
      altGal : 6,
    }
    
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
