import { Component, OnInit } from '@angular/core';
import { MiembrosService } from '../../services/miembros/miembros.service';


export interface cardsContent {
  cardTitle? : string | number;
  urlImg? : string;
  cardBody? : string;
  cardButton? : string;
  email? : string;
}

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.scss'],
  providers: [MiembrosService]
})
export class EquiposComponent implements OnInit {

miembros: any;

titleE = "Equipos";
subTitle = "Aquí vas a ver nuestros Equipos de Trabajo";
cardsContent : Array<cardsContent> = [
    {
      cardTitle : "Juventud",
      urlImg : "../../../../assets/Juventud.jpg",
      cardBody : "Nos encargamos de bridar una experiencia de participación activa, a través de encuentros y talleres para jóvenes",
      cardButton : "copasca.juventud@scouts.org.ar",
      email : "copasca.juventud@scouts.org.ar",
    },
    {
      cardTitle : "Comunicaciones",
      urlImg : "../../../../assets/Comunicaciones.jpg",
      cardBody : "Generamos contenido visuales, y compartimos los recursos conformado por los equipos",
      cardButton : "copasca.comunicaciones@scouts.org.ar",
      email : "copasca.comunicaciones@scouts.org.ar",
    },
    {
      cardTitle : "Laudato Si",
      urlImg : "../../../../assets/Laudato si.jpg",
      cardBody : "Nuestra pasión es la naturaleza, y en ella vivimos todos. Por eso te proponemos actividades para el cuidado de la casa común",
      cardButton : "copasca.laudatosi@scouts.org.ar",
      email : "copasca.laudatosi@scouts.org.ar",
    },
    {
      cardTitle : "Internacionales",
      urlImg : "../../../../assets/Internacionales.jpg",
      cardBody : "Generamos contenido visuales, y compartimos los recursos conformado por los equipos",
      cardButton : "copasca.internacionales@scouts.org.ar",
      email : "copasca.internacionales@scouts.org.ar",
    }
  ]

  constructor(
    private _miembrosServices: MiembrosService
  ) {
    this.getMiembros();
  }

  getMiembros(){
    this._miembrosServices.getMiembros().subscribe(
      response => {
        this.miembros = response.miembros;
      }, error => {
        console.log(error);
      }
      
    )
  }

  ngOnInit(): void {

  }

}