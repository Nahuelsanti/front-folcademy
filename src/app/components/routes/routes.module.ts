import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ServicesComponent } from './services/services.component';
import { EquiposComponent } from './equipos/equipos.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { TalleresComponent } from './talleres/talleres.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    HomeComponent,
    ContactComponent,
    ServicesComponent,
    EquiposComponent,
    GaleriaComponent,
    TalleresComponent,

  
  ],
  exports: [
    HomeComponent,
    ContactComponent,
    ServicesComponent,
    EquiposComponent,
    GaleriaComponent,
    TalleresComponent,

  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
    
  ]
})
export class RoutesModule { }
