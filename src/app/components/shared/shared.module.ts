import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './card/card.component';
/* import * as sharedComponent from './components/index' */



@NgModule({
  declarations: [
    /* ...sharedComponent.components */
    CardComponent
  ],
  exports: [
    /* ...sharedComponent.components */
    CardComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
