import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { cardTalleres } from '../../routes/talleres/talleres.component'; //Interfaz 

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  message: string;

  @Input() taller : cardTalleres;

  @Output() mensajeNewTaller = new EventEmitter<string>();

  constructor() {
    this.mensajeNewTaller = new EventEmitter
  }

  newTaller(){
    this.mensajeNewTaller.emit("Sea paciente, ¡Próximamente!")
  }

  ngOnInit(): void {
  }

}
