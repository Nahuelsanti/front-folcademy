import { Component, OnInit } from '@angular/core';

export interface contentSocial {
  urlSocial? : string,
  urlIcon? : string,
  altSocial? : string,
}

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

//Información
  titleInf = "Sobre Nosotros";
  contentInf = "La Comisión Pastoral Scout Católica es una Asociación Privada de Fieles, que nuclea a los scouts católicos que integran Scouts de Argentina, con Personería Jurídica Canónica otorgada por la Conferencia Episcopal Argentina. Es miembro de la CICE Conferencia Internacional Católica de Escultismo.";
  urlSaac = "https://www.scouts.org.ar/";
  imgSc = "../../../../assets/logoscoutarg.png";

//Redes Sociales
  titleSoc = "Nuestas Redes Sociales";
  contentSoc = "Enterate sobre las últimas novedades en:";
  contentSocial : Array <contentSocial> =[
    {
      urlSocial : "https://www.facebook.com/copasca/",
      urlIcon : "../../../../assets/facebook-square-brands.svg",
      altSocial : "Facebook",
    },
    {
      urlSocial : "https://www.instagram.com/copasca.arg/",
      urlIcon : "../../../../assets/instagram-square-brands.svg",
      altSocial : "Instagram",
    },
    {
      urlSocial : "https://www.youtube.com/channel/UCbLz2KM5QNF3OTiXW6D0CJA",
      urlIcon : "../../../../assets/youtube-brands.svg",
      altSocial : "Youtube",
    },
    {
      urlSocial : "https://issuu.com/copascanacional",
      urlIcon : "../../../../assets/book-open-solid.svg",
      altSocial : "Issu",
    }
  ]
  textSoc = "O escribinos en:";
  emailSoc = "copasca@scouts.org.ar";

//GoogleMaps
  textMaps = "Donde Encontrarnos";


  constructor() { }

  ngOnInit(): void {
  }

}
