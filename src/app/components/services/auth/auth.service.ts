import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    apiUrl: string = "https://identitytoolkit.googleapis.com/v1/";
    apiKey: "AIzaSyAGf-TyWNbc68SMvuK5ZvyhDsSS_Upehg0";

    constructor(private http: HttpClient) {}

    register(user: any): Observable<any> {
        let body = user;
        return this.http.post(this.apiUrl + "accounts:signUp?key=AIzaSyAGf-TyWNbc68SMvuK5ZvyhDsSS_Upehg0",body,{
        });
    }

    login(user: any): Observable<any> {
        let body = user;
        return this.http.post(this.apiUrl + "accounts:signInWithPassword?key=AIzaSyAGf-TyWNbc68SMvuK5ZvyhDsSS_Upehg0",body,{
        });
    }
}
//https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]