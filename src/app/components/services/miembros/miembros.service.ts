import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MiembrosService {

  baseUrl: string = 'http://demo2700559.mockable.io/';

  constructor(
    private _http: HttpClient
  ) { }

  getMiembros(): Observable<any> {
    
    return this._http.get(this.baseUrl + 'miembros');
  }
}
